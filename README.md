# DropBearTable Client-Facing Storefront

This submission implements a prototype of the client-facing website for the online restaurant-reservation 
service application, DropBearTable. 

## Usage

To start using this web application, open the index.html file in a web browser. 

## Implemented Features

### Login Link 

The 'Log In' button in the top right-hand side opens the window containing the login form. 
The login form is hardcoded to accept the email `name@example.com` with the password `password`. 

If the user enters any other login details an error message will appear under the 'Submit' button. 

The user can also close the window by clicking the close button on the upper right-hand side of the 
login window.

Once logged in, the login form window will close, and the link for the reservations page will 
appear in place of the 'Log In' button. Clicking the reservations page link, 'My Reservations', will 
open the reservations.html file. 

### Checking reservations

In further implementations of this project, this page will retrieve stored information about the 
user's bookings from the server. The user will also be able to edit details of different booking 
entries, such as the date and time, as well as be able to delete bookings. 

### Restaurant information page

From the index.html page, the pages for the three example restaurants can be accessed by clicking
the 'Booking Information' buttons within the information cards for each restaurant in the middle 
section of the homepage. 

Each restaurant page features three images of the example cuisine, along with some text information 
about the restaurant beneath it. 

### Checking availabilities

In the information page of each restaurant, the user can check for the availability of bookings for
specific dates, times, and their group's size. The calendar will show booking dates from the current day
to two months from then. 

Further implementations of this project will make booked dates/times unavailable to be selected by 
retrieving booking information from the server. 

Clicking the 'Check Availability' button will open the confirmation window that shows the restaurant, 
chosen date, time, and group size the user has selected. Below this is a form for the user to enter their
name, mobile number, and other details they would like to enter. 

After the user enters their details and clicks the 'Submit' button, the processing icon will appear for
five seconds while the page processes the booking. Afterwards, the processing icon will close, and the notification
window will appear, which confirms that the booking has been successful. The user can click the close button
on the upper right-hand side of the notification window to dismiss the message. 

### Contact us

The user has the option to send the DropBearTable team a message through the 'Contact Us' form in the 
bottom section of the index.html, and in each of the pages for the restaurants. 

The user can click the 'Contact Us' link on the upper right-hand side of each page to automatically scroll to the 
'Contact Us' form. Clicking this link from the reservations.html page will open the index.html page's 'Contact Us' 
form. 

### Chat bot

On each page is the chat bot button positioned on the lower right-hand side. Clicking this button will open 
the window for the chat bot messaging form. 

Future implementations of this window will show the messages from the DropBearTable server to help the user
retrieve their booking information. 

At the moment, users can type a message in the input form. Clicking the 'Send' button will display this message
in the chat window. 

Users can close the chat window at any time by clicking the close button on the upper right-hand side of the chat
window. 

## Author

Anna Mae Decelo ([adecelo@myune.edu.au](mailto:adecelo@myune.edu.au), student ID 220226741)

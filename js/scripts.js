(function () {
  "use strict";

  // MENU CLASS: Defines the functions for the menu div
  class Menu {
    constructor(containerId) {
      this._container = document.getElementById(containerId);
    }

    _buildMenu() {
      let chatButton = document.getElementById("chatBotDiv");
      let body = document.getElementById("docBody");

      // LOG IN LINK
      let logInLink = document.getElementById("logInLink");
      if (logInLink !== null) {
        logInLink.addEventListener("click", function () {
          logInDiv.classList.toggle("show-login");
          body.style.pointerEvents = "none";
          chatButton.style.pointerEvents = "none";
        });

        // LOG IN POPUP
        let logInDiv = document.getElementById("logInDiv");
        let emailInput = document.getElementById("emailInput");
        let passwordInput = document.getElementById("passwordInput");
        let logInSubmitBtn = document.getElementById("logInSubmitBtn");
        let invalidNote = document.getElementById("invalidNote");
        // Log in submit button functionality
        logInSubmitBtn.addEventListener("click", function () {
          // Hard code log in credentials as "name@example.com" and "password"
          if (emailInput.value.localeCompare("name@example.com") === 0 && passwordInput.value.localeCompare("password") === 0) {
            body.style.pointerEvents = "all";  // Restore button functionality of entire document
            chatButton.style.pointerEvents = "all";  // Restore chat button functionality
            invalidNote.classList.remove("invalid-entry");  // Remove error note if shown
            reservationsLink.classList.remove("hide-content");  // Show link to reservations after logging in successfully
            logInLink.classList.add("hide-content");  // Hide log in link after successful log in
            logInDiv.classList.toggle("show-login");  // Close log in window after successful log in
          }
          else {
            invalidNote.classList.add("invalid-entry");  // Show error note if email and password is incorrect
            logInSubmitBtn.style.borderColor = "#d02b2b";  // Red border on submit button with login error
            logInSubmitBtn.style.color = "#d02b2b";  // Red text on submit button with login error
          }
        })

        // Log in close window button
        let logInCloseBtn = document.getElementById("logInCloseBtn");
        logInCloseBtn.addEventListener("click", function () {
          invalidNote.classList.remove("invalid-entry");
          logInSubmitBtn.style.borderColor = "#c6c6c6";
          logInSubmitBtn.style.color = "#584141";
          body.style.pointerEvents = "all";
          chatButton.style.pointerEvents = "all";
          logInDiv.classList.toggle("show-login");
        });
        // END OF LOG IN POPUP

        this._container.appendChild(logInDiv);
      }

      // RESERVATIONS LINK
      let reservationsLink = document.getElementById("reservationsLink");
      reservationsLink.setAttribute("href", "reservations.html");

      // CONTACT US LINK
      let contactDiv = document.getElementById("contactUsDiv");
      let contactLink = document.getElementById("contactUsLink");
      if (contactLink !== null) {
        // Set contact us link outside of reservation page
        contactLink.addEventListener("click", function () {
          contactDiv.scrollIntoView();
        });
      } else {
        // Set contact us link within reservation page
        let contactReservation = document.getElementById("contactUsReservation");
        contactReservation.setAttribute("href", "index.html#contactUsDiv");
      }
    }

    render() {
      this._buildMenu();
    }
  } // CLOSING BRACKET OF MENU CLASS

  // BOOKING SEARCH CLASS: Defines the functions for the booking search bar
  class BookingSearch {
    getRestaurant() {
      return document.getElementById("restaurantName").innerHTML;
    }

    _buildSearchBar() {

      // TODO: RETRIEVE UNAVAILABLE DATES/TIMES FROM SERVER

      // CALENDAR PROPERTIES
      let calendarInput = document.querySelector('input[type="date"]');
      let currentDate = new Date().toISOString().slice(0, 10);
      let currentDay = String((new Date).getDate()).padStart(2, '0');
      let maxMonth = String((new Date).getMonth() + 3).padStart(2, '0');
      // Set default selection to current date
      calendarInput.setAttribute("value", currentDate);
      // Set minimum selectable date as current date
      calendarInput.setAttribute("min", currentDate);
      // Set maximum selectable date as two months from current date
      calendarInput.setAttribute("max", "2021-" + maxMonth + "-" + currentDay);

      // TIME OPTION PROPERTIES
      let timeInput = document.getElementById("timeInput");
      const timeOptionsList = ["10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30",
                           "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30",
                           "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30"];
      const timeOption = [];
      // Populate options for time selection
      for (let i = 0; i < timeOptionsList.length; i++) {
        timeOption.push(i);
        timeOption[i] = document.createElement("option");
        timeOption[i].innerHTML = timeOptionsList[i];
        timeOption[i].setAttribute("value", timeOptionsList[i]);
        timeInput.appendChild(timeOption[i]);
      }

      // NUMBER OF PEOPLE PROPERTIES
      let peopleInput = document.getElementById("peopleInput");
      const peopleOptionsList = ["1 Person", "2 People", "3 People", "4 People", "5 People", "6 People",
                                 "7 People", "8 People", "Larger Group"];
      const peopleOption = [];
      // Populate options for number of people selection
      for (let i = 0; i < peopleOptionsList.length; i++) {
        peopleOption.push(i);
        peopleOption[i] = document.createElement("option");
        peopleOption[i].innerHTML = peopleOptionsList[i];
        peopleOption[i].setAttribute("value", peopleOptionsList[i]);
        peopleInput.appendChild(peopleOption[i]);
      }
    }

    _checkAvailability() {
      let body = document.getElementById("docBody");  // Entire document body
      let restoValue = new BookingSearch().getRestaurant();  // Get restaurant name
      // Confirmation details html elements
      let confirmContainer = document.getElementById("confirmContainer");
      let restoName = document.getElementById("restoName");
      let dateInfo = document.getElementById("dateInfo");
      let timeInfo = document.getElementById("timeInfo");
      let peopleInfo = document.getElementById("peopleInfo");
      // Check availability button
      let checkBtn = document.getElementById("bookingBtn");
      checkBtn.addEventListener("click", function () {
        body.style.pointerEvents = "none";  // Turn off button clicks behind confirmation window
        restoName.innerHTML = "Restaurant selected: " + restoValue;  // Restaurant name
        dateInfo.innerHTML = "Date selected: " + document.getElementById("calendarInput").value;  // Date selected for booking
        timeInfo.innerHTML = "Time selected: " + document.getElementById("timeInput").value;  // Time selected for booking
        peopleInfo.innerHTML = "Number of guests selected: " + document.getElementById("peopleInput").value;  // No. of people selected for booking
        confirmContainer.classList.toggle("show-confirmation");  // Show confirmation window with booking details and form
      })

      // CONFIRMATION WINDOW
      let submitBookingBtn = document.getElementById("submitBookingBtn");
      let processingIcon = document.getElementById("processingContainer");
      let notifTitle = document.getElementById("notifTitle");
      let notifMessage = document.getElementById("notifMessage");
      // Submit booking button function
      submitBookingBtn.addEventListener("click", function () {
        confirmContainer.classList.toggle("show-confirmation");  // Close the confirmation window
        processingIcon.classList.toggle("show-icon");  // Show hourglass processing icon
        body.style.pointerEvents = "none";  // Turn off button clicks behind processing icon

        // TODO: ADD BOOKING INFO TO SERVER SIDE

        // BOOKING DETAILS
        let bookingEntry = document.createElement("div");  // Booking container
        bookingEntry.classList.add("booking-entry");
        let bookingTitle = document.createElement("h5");  // Booking title
        bookingTitle.classList.add("card-title");
        bookingTitle.innerHTML = "Booking " + (document.querySelectorAll(".booking-entry").length + 1);
        let restaurantBooking = document.createElement("p");  // Restaurant name
        restaurantBooking.classList.add("card-text");
        restaurantBooking.innerHTML = "Restaurant Name: " + restoValue;
        let status = document.createElement("p");  // Booking status
        status.classList.add("card-text");
        status.innerHTML = "Booking Status: Processing";
        let nameBooking = document.createElement("p");  // Guest name for booking
        nameBooking.classList.add("card-text");
        nameBooking.innerHTML = "Name: " + document.getElementById("username").value;
        let mobileNumber = document.createElement("p");  // Mobile number for booking
        mobileNumber.classList.add("card-text");
        mobileNumber.innerHTML = "Mobile Number: " + document.getElementById("mobile").value;
        let dateBooking = document.createElement("p");  // Date of booking
        dateBooking.classList.add("card-text");
        dateBooking.innerHTML = "Date: " + document.getElementById("calendarInput").value;
        let timeBooking = document.createElement("p");  // Time of booking
        timeBooking.classList.add("card-text");
        timeBooking.innerHTML = "Time: " + document.getElementById("timeInput").value;
        let peopleBooking = document.createElement("p");  // Number of guests
        peopleBooking.classList.add("card-text");
        peopleBooking.innerHTML = "Number of Guests: " + document.getElementById("peopleInput").value;
        let additionalNotes = document.createElement("p");  // Additional info/special requests
        additionalNotes.classList.add("card-text");
        additionalNotes.innerHTML = "Special Requests: " + document.getElementById("specialRequests").value;
        bookingEntry.append(bookingTitle, restaurantBooking, status, nameBooking, mobileNumber, dateBooking, timeBooking, peopleBooking, additionalNotes);
        // END OF BOOKING DETAILS

        // Update notification message with booking details
        notifMessage.innerHTML = "Thank you, " + document.getElementById("username").value + "! Booking created with " + restoValue + " on " + document.getElementById("calendarInput").value + " at " + document.getElementById("timeInput").value + " for " + document.getElementById("peopleInput").value;
        // Complete processing of booking after 5 seconds
        setTimeout(function(){
          processingIcon.classList.toggle("show-icon");  // Close the hourglass processing icon
          notifContainer.classList.toggle("show-notification");  // Show the notification window confirming booking
          status.innerHTML = "Booking Status: Confirmed";  // Update booking status to confirmed
        }, 5000);
      })

      // Close button for confirmation window
      let confirmationClose = document.getElementById("confirmationClose");
      confirmationClose.addEventListener("click", function () {
        confirmContainer.classList.toggle("show-confirmation");
        body.style.pointerEvents = "all";
      });

      // Close button for notification window
      let notifContainer = document.getElementById("notifContainer");
      let notifClose = document.getElementById("notifClose");
      notifClose.addEventListener("click", function () {
        notifContainer.classList.toggle("show-notification");
        body.style.pointerEvents = "all";
      })
    }

    render() {
      this._buildSearchBar();
    }

    renderConfirmation() {
      this._checkAvailability();
    }
  } // CLOSING BRACKET OF BOOKING SEARCH CLASS

  // CONTACT US CLASS: Defines the functions for the contact us form
  class ContactUs {
    _buildContactUs() {
      let contactEmail = document.getElementById("contactUsEmail");
      let contactMessage = document.getElementById("contactUsMessage");
      let contactSubmitBtn = document.getElementById("contactUsSubmit");
      contactSubmitBtn.addEventListener("click", function () {
        console.log("Contact email: " + contactEmail.value);
        console.log("Contact message: " + contactMessage.value);
      })
    }

    render() {
      this._buildContactUs();
    }
  } // CLOSING BRACKET OF CONTACT US CLASS

  // CHAT BOT CLASS: Defines the functions for the chat bot
  class ChatBot {
    _buildChatBot() {
      let body = document.getElementById("docBody");
      let chatWindow = document.getElementById("chatWindow");
      let messageDisplay = document.getElementById("messageDisplay");
      let processingIcon = document.getElementById("processingContainer");
      let botInput = document.createElement("div");
      let chatButton = document.getElementById("chatBotDiv");
      // Function for chat icon to open chat window
      chatButton.addEventListener("click", function () {
        body.style.pointerEvents = "none";
        chatButton.style.pointerEvents = "none";
        chatCloseBtn.style.pointerEvents = "none";
        chatWindow.classList.toggle("show-chat");  // Show chat window
        processingIcon.classList.toggle("show-icon");  // Show processing icon when chat opens
        setTimeout(function(){
          chatCloseBtn.style.pointerEvents = "all";
          processingIcon.classList.toggle("show-icon");
          botInput.innerHTML = "DropBear Bot: Hi! How can I help?";  // Load the bot welcome message when chat window opens
          messageDisplay.appendChild(botInput);  // Delay loading of bot welcome message
        }, 2000);
      })
      // Function for the send button of chat
      let chatTextInput = document.getElementById("chatTextInput");
      let chatSendBtn = document.getElementById("chatSendBtn");
      chatSendBtn.addEventListener("click", function () {
        let userInput = chatTextInput.value;  // Get message entered by the user
        let userMessage = document.createElement("div");
        userMessage.innerHTML = "User: " + userInput;
        chatTextInput.value = "";
        messageDisplay.appendChild(userMessage); // Display message entered by the user
      })
      // Function for close button of chat window
      let chatCloseBtn = document.getElementById("chatCloseBtn");
      chatCloseBtn.addEventListener("click", function () {
        body.style.pointerEvents = "all";
        chatTextInput.value = "";
        chatButton.style.pointerEvents = "all";
        chatWindow.classList.toggle("show-chat");
      })
    }

    render() {
      this._buildChatBot();
    }
  } // CLOSING BRACKET OF CHAT BOT CLASS

  // RENDER THE MENU BUTTON AND MENU ITEMS
  const menuContainer = "menu";
  let menuObject = new Menu(menuContainer);
  menuObject.render();

  // RENDER THE MAIN BODY
  let bookingObject = new BookingSearch();
  if (document.querySelector('input[type="date"]') !== null) {
    bookingObject.render();
  }
  if (document.getElementById("confirmationClose") !== null) {
    bookingObject.renderConfirmation();
  }

  // RENDER THE CONTACT US DIV
  let contactUsObject = new ContactUs();
  if (document.getElementById("contactUsSubmit") !== null) {
    contactUsObject.render();
  }

  // RENDER THE CHAT BOT DIV
  let chatObject = new ChatBot();
  chatObject.render();
}());